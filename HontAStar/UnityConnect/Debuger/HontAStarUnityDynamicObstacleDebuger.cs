﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

namespace Hont.AStar
{
    [RequireComponent(typeof(HontAStarUnity))]
    public class HontAStarUnityDynamicObstacleDebuger : MonoBehaviour
    {
        public HontAStarUnity astar;


        void Awake()
        {
            astar = GetComponent<HontAStarUnity>();
        }

        void Update()
        {
            if (astar != null && astar.Grid != null)
            {
                foreach (Position item in astar.Grid)
                {
                    var node = astar.Grid.GetUserData(item) as HontAStarUnityDebugNode;
                    node.isWalkable = astar.Grid.GetIsWalkable(node.AStarPosition);
                }
            }
        }
    }
}
